import {Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import {OwlOptions} from 'ngx-owl-carousel-o';


interface ClimbTab {
  name: string;
  bgUrl: string;
  isActive: boolean;
  dates: Array<{
    date: string;
    title: string;
  }>;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  @ViewChild('secondSection') secondSection: ElementRef;
  @ViewChild('thirdSection') thirdSection: ElementRef;
  @ViewChild('header') header: ElementRef;

  customOptions: OwlOptions = {
    loop: true,
    dots: true,
    navSpeed: 700,
    margin: 10,
    nav: false,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    }
  };
  tabs: ClimbTab[] = [
    {
      name: 'mountain 1',
      bgUrl: 'url(/assets/img/mountain1.jpg) center no-repeat fixed',
      isActive: true,
      dates: [
        {date: '25 Nov 2016', title: 'Vestibulum viverra'},
        {date: '28 Nov 2016', title: 'Vestibulum viverra'},
        {date: '', title: ''},
        {date: '18 Dec 2016', title: 'Vestibulum viverra'},
        {date: '7 Jan 2016', title: 'Vestibulum viverra'}
      ]
    },
    {
      name: 'mountain 2',
      bgUrl: 'url(/assets/img/mountain2.jpg) center no-repeat fixed',
      isActive: false,
      dates: [
        {date: '17 Nov 2016', title: 'Vestibulum viverra'},
        {date: '', title: ''},
        {date: '13 Dec 2016', title: 'Vestibulum viverra'},
        {date: '28 Dec 2016', title: 'Vestibulum viverra'},
        {date: '', title: ''},
        {date: '9 Feb 2017', title: 'Vestibulum viverra'}
      ]
    }
  ];


  @HostListener('window:scroll')
  onWindowScroll() {
    if (window.pageYOffset > 1) {
      this.header.nativeElement.classList.add('active');
    } else {
      this.header.nativeElement.classList.remove('active');
    }
  }

  onScrollTo(event, element): void {
    event.preventDefault();

    const yOffset = -this.header.nativeElement.clientHeight;
    const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;

    window.scrollTo({top: y, behavior: 'smooth'});
  }

  onTabChange(event, tab: ClimbTab): void {
    event.preventDefault();
    this.tabs.forEach(item => item.isActive = false);
    tab.isActive = true;
  }
}
